﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;
using System.Collections.Generic;
using Circustrein_2_Classes.Classes;
using Circustrein_2_Classes.Enums;

namespace CircustreinTests;

public class WagonTests
{
	[Fact]
	public void CanAddAnimal_ReturnTrue()
	{
		//arrange
		Wagon wagon = new Wagon();
		Animal animal = new Animal(AnimalSize.Small, AnimalEaterType.Herbivore);
		//act
		bool result = wagon.CanAddAnimal(animal, wagon);

		//assert
		result.Should().BeTrue();	
	}

	[Theory]
	[InlineData(5)]
	[InlineData(6)]
	[InlineData(7)]
	public void CanAddAnimal_ShouldReturnFalse_WhenExceedsSize(int space)
	{
		//arrange
		Wagon wagon = new Wagon();
		wagon.wagonSpace = space;
		Animal newAnimal = new Animal(AnimalSize.Large, AnimalEaterType.Herbivore);

		//act
		bool result = wagon.CanAddAnimal(newAnimal, wagon);

		//assert
		result.Should().BeFalse();
	}

	public void CanAddAnimal_ShouldReturnFalse_WhenCarnivoreInWagon()
	{
		//arrange
		Wagon wagon = new Wagon();
		Animal existingCarnivore = new Animal(AnimalSize.Medium, AnimalEaterType.Carnivore);
		wagon.animals.Add(existingCarnivore);

		Animal newAnimal = new Animal(AnimalSize.Small, AnimalEaterType.Carnivore);

		//act
		bool result = wagon.CanAddAnimal(newAnimal, wagon);

		//assert
		result.Should().BeFalse();
	}

	[Fact]
	public void OptimizeAnimalsInWagons_ShouldReturnOptimizedWagons()
	{
		//arrange
		Wagon wagon = new Wagon();
		List<Animal> animals = new List<Animal>
			{
				new Animal(AnimalSize.Small, AnimalEaterType.Herbivore),
				new Animal(AnimalSize.Medium, AnimalEaterType.Herbivore),
				new Animal(AnimalSize.Large, AnimalEaterType.Herbivore),
				new Animal(AnimalSize.Small, AnimalEaterType.Carnivore),
				new Animal(AnimalSize.Medium, AnimalEaterType.Carnivore),
				new Animal(AnimalSize.Large, AnimalEaterType.Carnivore)
			};

		List<Wagon> wagons = wagon.FillWagons(animals);

		//act
		List<Wagon> result = wagon.OptimizeAnimalsInWagons(wagons);

		//assert
		result.Should().NotBeNull();

		foreach (Wagon optimizedWagon in result)
		{
			optimizedWagon.wagonSpace.Should().NotBe(0);
		}
	}
}
