using Circustrein_2_Classes;
using Circustrein_2_Classes.Classes;
using Circustrein_2_Classes.Enums;
using Circustrein_2_Classes.Testing;
using FluentAssertions;
using Xunit;
using System.Collections.Generic;

namespace CircustreinTests;

public class AnimalTests
{
    [Fact]
    public void Animal_GetAnimalBySizeAndEaterTypeAndAmount_ReturnListOneAnimalSmallCarnivore()
    {
        //arrange
        AnimalSize animalSize = AnimalSize.Small;
        AnimalEaterType animalEaterType = AnimalEaterType.Carnivore;

        List<Animal> expectedResult = new List<Animal>
        {
            new Animal(animalSize, animalEaterType)
        };

        Animal animal = new Animal();
        int animalAmount = 1;

        //act
        List<Animal> actualAnimals = animal.GetAnimalBySizeAndEaterTypeAndAmount(
            animalSize, 
            animalEaterType, 
            animalAmount);

        //assert
        actualAnimals.Should().BeEquivalentTo(expectedResult);
    }

    [Fact]
    public void Animal_GetAnimalAmountFromUserBySizeAndType_ReturnInt()
    {
        //arrange
        string consoleInput = "1";
        StringReader stringReader = new StringReader(consoleInput);
        Console.SetIn(stringReader);

        int expectedResult = 1;

        Animal animal = new Animal(Console.In);

        AnimalSize animalSize = AnimalSize.Small;
        AnimalEaterType animalEaterType = AnimalEaterType.Carnivore;

        //act
        int actualResult = animal.GetAnimalAmountFromUserBySizeAndEaterType(animalSize, animalEaterType);

        //assert
        actualResult.Should().Be(expectedResult);
    }

    [Fact]
    public void Animal_GetAllAnimals_ReturnListAnimals()
    {
        //arrange
        List<Animal> expectedAnimals = new List<Animal>
        {
            new Animal(AnimalSize.Small, AnimalEaterType.Carnivore),
            new Animal(AnimalSize.Medium, AnimalEaterType.Carnivore),
            new Animal(AnimalSize.Large, AnimalEaterType.Carnivore),
            new Animal(AnimalSize.Small, AnimalEaterType.Herbivore),
            new Animal(AnimalSize.Medium, AnimalEaterType.Herbivore),
            new Animal(AnimalSize.Large, AnimalEaterType.Herbivore),
        };

        int[] animalAmounts = { 1, 1, 1, 1, 1, 1 };

        Animal animal = new Animal();

        //act
        List<Animal> actualAnimals = animal.GetAllAnimals(animalAmounts);

        //assert
        actualAnimals.Should().BeEquivalentTo(expectedAnimals);
    }

    [Theory]
    [InlineData(new int[Animal.maxAnimalCombinations] { 1, 0, 0, 0, 3, 2 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 1, 0, 0, 5, 2, 1 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 1, 1, 1, 1, 1, 1 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 2, 1, 1, 1, 5, 1 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 1, 0, 0, 1, 1, 2 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 3, 0, 0, 0, 2, 3 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 7, 3, 3, 0, 5, 6 })]
    [InlineData(new int[Animal.maxAnimalCombinations] { 0, 0, 0, 0, 0, 0 })]
    public void Animal_GetAllAnimals_ReturnsListAnimals_TestCases(int[] inputAmounts)
    {
        //arrange
        Animal animal = new Animal();
        List<Animal> expectedAnimals = new List<Animal>();

        //kon ook GetAllAnimals gebruiken, maar voelt wat dubbel
        //deze test test of verschillende combinaties van dieren toevoegen werkt
        for (int i = 0; i < Animal.maxAnimalCombinations; i++)
        {
            expectedAnimals.AddRange(animal.GetAnimalBySizeAndEaterTypeAndAmount(
                animal.animalCombinations[i].size, 
                animal.animalCombinations[i].type, 
                inputAmounts[i]));
        }
        //act
        List<Animal> actualAnimals = animal.GetAllAnimals(inputAmounts);

        //assert
        actualAnimals.Should().BeEquivalentTo(expectedAnimals);
    }
}
