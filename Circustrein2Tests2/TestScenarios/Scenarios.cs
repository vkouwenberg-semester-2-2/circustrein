﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using Circustrein_2_Classes;
using Circustrein_2_Classes.Classes;
using Circustrein_2_Classes.Enums;
using Circustrein_2_Classes.Testing;
using Circustrein_2_Classes.Logic;
using System.Security.Cryptography.X509Certificates;

namespace Circustrein2Tests2.TestScenarios
{
    [TestClass]
    public class Scenarios
    {
		private readonly WagonLogic _wagonLogic;
		private readonly AnimalLogic _animalLogic;
		private readonly TestSetup _testSetup;

		public Scenarios(WagonLogic wagonLogic, AnimalLogic animalLogic, TestSetup testSetup)
		{
			_wagonLogic = wagonLogic;
			_animalLogic = animalLogic;
			_testSetup = testSetup;
		}

        [TestMethod]
        public void Scenario()
        {
            List<Animal> animals = _animalLogic.EnterAllAnimals();
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
        }

		[TestMethod]
        public void Scenario1()
        {
			List<Animal> animals = _testSetup.EnterAllTestAnimals(1, 0, 0, 0, 3, 2);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
		[TestMethod]
		public void Scenario2()
		{
			List<Animal> animals = _testSetup.EnterAllTestAnimals(1, 0, 0, 5, 2, 1);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
		[TestMethod]
		public void Scenario3()
		{
			List<Animal> animals = _testSetup.EnterAllTestAnimals(1, 1, 1, 1, 1, 1);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
        [TestMethod]
        public void Scenario4()
        {
			List<Animal> animals = _testSetup.EnterAllTestAnimals(2, 1, 1, 1, 5, 1);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
		[TestMethod]
		public void Scenario5()
		{
			List<Animal> animals = _testSetup.EnterAllTestAnimals(1, 0, 0, 1, 1, 2);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
		[TestMethod]
		public void Scenario6()
		{
			List<Animal> animals = _testSetup.EnterAllTestAnimals(3, 0, 0, 0, 2, 3);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
		[TestMethod]
		public void Scenario7()
		{
			List<Animal> animals = _testSetup.EnterAllTestAnimals(7, 3, 3, 0, 5, 6);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
		[TestMethod]
		public void Scenario8()
		{
			List<Animal> animals = _testSetup.EnterAllTestAnimals(0, 0, 0, 0, 0, 0);
			List<Wagon> wagons = _wagonLogic.FillWagons(animals);
			_wagonLogic.OptimizeAnimalsInWagons(wagons);
		}
	}
}