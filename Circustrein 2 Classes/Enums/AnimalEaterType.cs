﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Circustrein_2_Classes.Enums
{
    public enum AnimalEaterType
    {
        Carnivore,
        Herbivore
    }
}
