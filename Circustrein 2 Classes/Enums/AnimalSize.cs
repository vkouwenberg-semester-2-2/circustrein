﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Circustrein_2_Classes.Enums
{
    public enum AnimalSize
    {
        Small = 1,
        Medium = 3,
        Large = 5
    }
}
