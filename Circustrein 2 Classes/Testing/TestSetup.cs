﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Circustrein_2_Classes.Classes;
using Circustrein_2_Classes.Enums;

namespace Circustrein_2_Classes.Testing;

public class TestSetup
{
    public List<Animal> GetTestAnimal(AnimalSize animalSize, AnimalEaterType eaterType, int amount)
    {
        int animalAmount = amount;

        List<Animal> animals = new List<Animal>();

        for (int i = 0; i < animalAmount; i++)
        {
            animals.Add(new Animal(animalSize, eaterType));
        }
        return animals;
    }
    public List<Animal> EnterAllTestAnimals(int sC, int mC, int lC, int sH, int mH, int lH)
    {
        List<Animal> animals = new List<Animal>();
        animals.AddRange(GetTestAnimal(AnimalSize.Small, AnimalEaterType.Carnivore, sC));
        animals.AddRange(GetTestAnimal(AnimalSize.Medium, AnimalEaterType.Carnivore, mC));
        animals.AddRange(GetTestAnimal(AnimalSize.Large, AnimalEaterType.Carnivore, lC));
        animals.AddRange(GetTestAnimal(AnimalSize.Small, AnimalEaterType.Herbivore, sH));
        animals.AddRange(GetTestAnimal(AnimalSize.Medium, AnimalEaterType.Herbivore, mH));
        animals.AddRange(GetTestAnimal(AnimalSize.Large, AnimalEaterType.Herbivore, lH));
		

		return animals;
    }
}
