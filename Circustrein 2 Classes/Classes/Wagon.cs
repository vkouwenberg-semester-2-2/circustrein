﻿using Circustrein_2_Classes.Enums;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace Circustrein_2_Classes.Classes;

public class Wagon
{
    public List<Animal> animals;
    public int wagonSpace;
    private const int _maxSpace = 10;


    public Wagon()
    {
        animals = new List<Animal>();
        wagonSpace = 0;
    }

    public List<Wagon> GetFilledAndFilteredWagons()
    {
        Wagon wagon = new Wagon();
        Animal animal = new Animal();

        int[] animalAmounts = animal.GetAllAnimalAmounts();
        List<Animal> animals = animal.GetAllAnimals(animalAmounts);
        List<Wagon> wagons = wagon.FillWagons(animals);
        wagon.OptimizeAnimalsInWagons(wagons);
        return wagons;
    }

    public List<Wagon> OptimizeAnimalsInWagons(List<Wagon> wagons)
    {
        List<Wagon> optimizedWagons = new List<Wagon>();

        //same as in CanAddAnimal
        List<Wagon> descendingWagons = wagons.OrderByDescending(wagon => wagon.wagonSpace).ToList();

        foreach (Wagon wagon in descendingWagons)
        {
            //All checks ALL things in a collection are the same
            //What it checks is if animal.Type == herbivore
            bool herbivoresOnlyWagon = wagon.animals.All(animal => animal.Type == AnimalEaterType.Herbivore);

            if (herbivoresOnlyWagon == true)
            {
                Wagon optimizedWagon = new Wagon();

                foreach (Animal animal in wagon.animals)
                {
                    if (CanAddAnimal(animal, optimizedWagon) == true)
                    {
                        optimizedWagon.animals.Add(animal);
                        optimizedWagon.wagonSpace += (int)animal.Size;
                    }
                }

                optimizedWagons.Add(optimizedWagon);
            }
            else
            {
                optimizedWagons.Add(wagon);
            }
        }

        return optimizedWagons;
    }

    public List<Wagon> FillWagons(List<Animal> animals)
    {
        List<Wagon> wagons = new List<Wagon>();
        List<Animal> herbivores = new List<Animal>();
        //List<Animal> descendingHerbivores = new List<Animal>();

        //place carnivores in seperate wagons
        foreach (Animal animal in animals)
        {
            if (animal.Type == AnimalEaterType.Carnivore)
            {
                Wagon wagon = new Wagon();
                wagon.animals.Add(animal);
                wagon.wagonSpace += (int)animal.Size;
                wagons.Add(wagon);
            }
            else
            {
                herbivores.Add(animal);
            }
        }

        //a => a.Size means animal => animal.Size
        //they are filtered according to OrderByDescending
        //OrderByDescending returns IOrderedEnumerable<>, so since I request a List, .ToList
        List<Animal> descendingHerbivores = herbivores.OrderByDescending(animal => animal.Size).ToList();
        List<Wagon> wagonsToAdd = new List<Wagon>();

        foreach (Animal animal in descendingHerbivores)
        {
            bool addedToExistingWagon = false;
            //checks if herbivore can be added to existing wagons
            foreach (Wagon wagon in wagons)
            {
                if (CanAddAnimal(animal, wagon))
                {
                    wagon.animals.Add(animal);
                    wagon.wagonSpace += (int)animal.Size;
                    addedToExistingWagon = true;
                    break;
                    //succesfully added
                }
            }

            //no space in current wagons. Need to make new wagon
            if (!addedToExistingWagon)
            {
                Wagon wagonToAdd = new Wagon();
                wagonToAdd.animals.Add(animal);
                wagonToAdd.wagonSpace += (int)animal.Size;
                wagonsToAdd.Add(wagonToAdd);
            }
        }

        wagons.AddRange(wagonsToAdd);

        return wagons;
    }

    public bool CanAddAnimal(Animal newAnimal, Wagon wagon)
    {
        bool isNewAnimalCarnivore = IsNewAnimalCarnivore(newAnimal);
        bool doesWagonHaveCarnivore = DoesWagonHaveCarnivore(wagon);

        if (!CanAddAnimalSize(newAnimal, wagon))
        {
            return false;
        }

        if (isNewAnimalCarnivore && doesWagonHaveCarnivore)
        {
            return false;
        }

        if (isNewAnimalCarnivore && !doesWagonHaveCarnivore)
        {
            if (AreAllHerbivoresBigger(newAnimal, wagon))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        return true;
    }

    private bool CanAddAnimalSize(Animal newAnimal, Wagon wagon)
    {
        int totalSize = (int)newAnimal.Size + wagon.wagonSpace;

        if (totalSize >= _maxSpace)
        {
            return false;
        }
        else return true;
    }

	private bool DoesWagonHaveCarnivore(Wagon wagon)
    {
        if (wagon.animals.Any(animal => animal.Type == AnimalEaterType.Carnivore))
        {
            return true;
        }
        else return false;
	}

    private bool IsNewAnimalCarnivore(Animal newAnimal)
    {
        if (newAnimal.Type == AnimalEaterType.Carnivore)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

	private bool AreAllHerbivoresBigger(Animal newAnimal, Wagon wagon)
	{
		foreach (Animal animal in wagon.animals)
		{
			if (newAnimal.Type == AnimalEaterType.Herbivore && (int)newAnimal.Size >= (int)animal.Size)
			{
				return false;
			}
		}
		return true;
	}



    public void ListWagons(List<Wagon> wagons)
    {
        Console.WriteLine("There are {0} wagons", wagons.Count);

        if (wagons == null || wagons.Any())
        {
            int wagonCount = 0;

            foreach (Wagon wagon in wagons)
            {
                int animalCount = wagon.animals.Count;
                Console.WriteLine("This is wagon {0}, which has {1} animals", wagonCount, animalCount);
                Console.WriteLine("Capacity is at {0} / 10", wagon.wagonSpace);
                foreach (Animal animal in wagon.animals)
                {
                    string eaterType = animal.Type.ToString();
                    string size = animal.Size.ToString();
                    Console.WriteLine("{0}, {1}", eaterType, size);
                }
            }
        }

        Console.WriteLine("All wagons listed");
    }
}
