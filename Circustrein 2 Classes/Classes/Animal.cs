﻿using Circustrein_2_Classes.Enums;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Circustrein_2_Classes.Classes;

public class Animal
{
    public AnimalSize Size;
    public AnimalEaterType Type;

    public TextReader inputReader = Console.In;

    public const int maxAnimalCombinations = 6;

    public (AnimalSize size, AnimalEaterType type)[] animalCombinations =
    {
        (AnimalSize.Small, AnimalEaterType.Carnivore),
        (AnimalSize.Medium, AnimalEaterType.Carnivore),
        (AnimalSize.Large, AnimalEaterType.Carnivore),
        (AnimalSize.Small, AnimalEaterType.Herbivore),
        (AnimalSize.Medium, AnimalEaterType.Herbivore),
        (AnimalSize.Large, AnimalEaterType.Herbivore)
    };

    public Animal(AnimalSize size, AnimalEaterType type)
    {
        Size = size;
        Type = type;
    }

    public Animal()
    {

    }

    public Animal(TextReader reader)
    {
        inputReader = reader;
    }

    public List<Animal> GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize animalSize, AnimalEaterType eaterType, int animalAmount)
    {


        List<Animal> animals = new List<Animal>();

        for (int i = 0; i < animalAmount; i++)
        {
            animals.Add(new Animal(animalSize, eaterType));
        }

        return animals;
    }

    public int GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize animalSize, AnimalEaterType animalEaterType)
    {
        int animalAmount = 0;
        Console.WriteLine("How many {0} {1}? (Enter a number)", animalSize.ToString(), animalEaterType.ToString());
        try
        {
            animalAmount = Convert.ToInt32(inputReader.ReadLine());
        }
        catch
        {
            Console.ForegroundColor = ConsoleColor.DarkRed;
            Console.WriteLine("Error, please enter a number!");
            Console.ResetColor();
        }

        return animalAmount;
    }

    public int[] GetAllAnimalAmounts()
    {
        int[] animalAmounts = new int[maxAnimalCombinations];

        for (int i = 0; i < maxAnimalCombinations; i++)
        {
            animalAmounts[i] = GetAnimalAmountFromUserBySizeAndEaterType(
                animalCombinations[i].size,
                animalCombinations[i].type);
        }

        return animalAmounts;
    }

    public List<Animal> GetAllAnimals(int[] animalAmounts)
    {
        Animal animal = new Animal();
        List<Animal> animals = new List<Animal>();
        
        for (int i = 0; i < maxAnimalCombinations; i++)
        {
            animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(
                animalCombinations[i].size, 
                animalCombinations[i].type, 
                animalAmounts[i]));
        }

        //Create the animal(s)
        /*animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize.Large, AnimalEaterType.Herbivore,
            animal.GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize.Large, AnimalEaterType.Herbivore)));

        animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize.Medium, AnimalEaterType.Herbivore,
            animal.GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize.Medium, AnimalEaterType.Herbivore)));

        animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize.Small, AnimalEaterType.Herbivore,
            animal.GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize.Small, AnimalEaterType.Herbivore)));

        animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize.Large, AnimalEaterType.Carnivore,
            animal.GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize.Large, AnimalEaterType.Carnivore)));

        animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize.Medium, AnimalEaterType.Carnivore,
            animal.GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize.Medium, AnimalEaterType.Carnivore)));

        animals.AddRange(GetAnimalBySizeAndEaterTypeAndAmount(AnimalSize.Small, AnimalEaterType.Carnivore,
            animal.GetAnimalAmountFromUserBySizeAndEaterType(AnimalSize.Small, AnimalEaterType.Carnivore)));
*/
        return animals;
    }
}



